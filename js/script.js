/*
    It`s free library
    done Gorkovenko Dmitry, gorkovenko.dmitry@gmail.com
*/
var tabs = [
    {val: 3, name: 'Параметры'}, //параметры графиков
    {val: 1, name: 'Информация'},
    {val: 5, name: 'Настройки сети'}
];
var edited={};
var curr_graph=false;
var curr_interval = false;
var RefreshPeriod=2;
var btn_close=document.getElementById('close');
btn_close.onclick=function(e) {
    var error_div=document.getElementById('error_div');
    error_div.add_class('hide');
    clearTimeout(close_interval);
};
var close_interval;
function check_error(data) {
    try {
        var obj=JSON.parse(data);
    }
    catch (e) {
        return false;
    }
    try {
        if (obj[0]=='alert') {
            var error_div=document.getElementById('error_div');
            var error_span=error_div.getElementsByTagName('span')[0];
            error_span.innerHTML=obj[4];
            error_div.remove_class('hide');
            var t=parseInt(obj[1]);
            if (t>0) {
                close_interval=setTimeout(function() {
                    error_div.add_class('hide');
                }, t*1000);
            }
            var color=parseInt(obj[2]);
            var color_div=document.getElementById('error_text');
            switch (color) {
                case 3:
                    color_div.className='red';
                    break;
                case 5:
                    color_div.className='yellow';
                    break;
                case 7:
                    color_div.className='green';
                    break;
                case 8:
                    color_div.className='blue';
                    break;
            }
            return false;
        }
        else {
            return obj;
        }
    }
    catch (e) {
        console.log('подумать');
        return false;
    }
}
function get_and_draw(graph, ignore_zoom) {
    if (!ignore_zoom) {
        if (!graph.auto_refresh) {
            return;
        }
    }
    ajax({
        url: graph.data_url,
        type: 'GET',
        data: '',
        success: function(data) {
            try {
                var obj = check_error(data);
                if (!obj) {
                    return;
                }
                var graph_data = obj['spectrum'];
                var res_data = [];
                var i;

                var num_graph=parseInt(graph.id.substr(5));
                var INGrid=false;
                try {
                    INGrid=tabs[0].params['IN'+num_graph+'Grid'][0];
                }
                catch (e) {
                    INGrid=false;
                }
                var button_grid = document.getElementsByName('grid');
                var grid_val;
                for (i=0; i<button_grid.length; i++) {
                    if (INGrid) {
                        button_grid[i].checked=(INGrid==button_grid[i].value);
                    }
                    if (button_grid[i].checked) {
                        grid_val=button_grid[i].value;
                    }
                }
                var INLaMin=false;
                var INLaMax=false;
                try {
                    INLaMin=tabs[0].params['IN'+num_graph+'LaMin'][0];
                    INLaMax=tabs[0].params['IN'+num_graph+'LaMax'][0];
                }
                catch (e) {
                    INLaMax=INLaMin=false;
                }
                for (i=0; i<graph_data.length; i++) {
                    var el=[];
                    if (INLaMin && INLaMax) {
                        if (graph_data[i][1]>=INLaMin && graph_data[i][1]<=INLaMax) {
                            el[0]=graph_data[i][1];
                            el[1]=graph_data[i][2];
                            res_data.push(el);
                        }
                    }
                    else {
                        el[0]=graph_data[i][1];
                        el[1]=graph_data[i][2];
                        res_data.push(el);
                    }
                }
                graph.data = res_data;
                graph.old_data = res_data;
                graph.grid_val = grid_val;
                graph.clear_rect();
                graph.draw();

                var table_data = obj['analysis'];
                var table_el = document.getElementById('data_table');
                var table_body = table_el.getElementsByTagName('tbody')[0];
                var res_text = '';
                var channel_num = 1;
                var channel_name = '';
                var f, l;
                for (i=0; i<table_data.length; i++) {
                    f=190.1+i*0.05;
                    l=299792.458/f;
                    if (i%2==0) {
                        channel_name = 'C' + channel_num;
                    }
                    else {
                        channel_name = 'H' + channel_num;
                        channel_num++;
                    }
                    if (grid_val==1 && i%2!=0) {
                        continue;
                    }
                    res_text += '<tr><td>'+channel_name+'</td><td>'+ f.toFixed(2)+'</td><td>'+ l.toFixed(2)+'</td><td>'+table_data[i][1]+'</td><td>'+table_data[i][2]+'</td><td>'+table_data[i][3]+'</td></tr>';
                }
                if (res_text) {
                    table_body.innerHTML = res_text;
                }
                else {
                    table_body.innerHTML = '<tr><td colspan="6">Нет данных</td></tr>';
                }
            }
            catch (e) {
                console.log('ошибка загрузки данных');
            }
        }
    });
}
HTMLElement.prototype.add_class=function(class_name) {
    if (this.className.indexOf(class_name)==-1) {
        this.className=this.className.trim();
        this.className+=' '+class_name;
        this.className=this.className.trim();
    }
};
HTMLElement.prototype.remove_class=function(class_name) {
    this.className=this.className.replace(class_name, '');
    this.className=this.className.trim();
};
function main() {
    var min_max_div=document.getElementById('min_max_div');
    var canvas_list=min_max_div.getElementsByTagName('canvas');
    for (var i=0; i<canvas_list.length; i++) {
        canvas_list[i].width=min_max_div.offsetWidth;
    }
    function zoom_action() {
        var button_set_auto = document.getElementById('set_auto');
        button_set_auto.checked=false;
    }
    curr_graph = new Graph({
        id: 'graph1',
        tick_x: 40,
        tick_y: 5,
        zoom_action: zoom_action
    });
    get_and_draw(curr_graph);
    curr_graph.auto_refresh=true;
    curr_interval = setInterval(function() {
        get_and_draw(curr_graph);
    }, RefreshPeriod*1000);

    var graph_list=['graph1', 'graph2', 'graph3', 'graph4'];
    var graph_tabs = document.getElementById('graph_tabs');
    var ul_tabs = document.getElementById('tabs');
    var links = ul_tabs.getElementsByTagName('a');
    function lnk_click(e) {
        if (e.preventDefault) {  // если метод существует
            e.preventDefault();
        } else { // вариант IE<9:
            e.returnValue = false;
        }
        reset_params();
        for (var i=0; i<tabs_arr.length; i++) {
            tabs_arr[i].setAttribute('class', '');
        }
        for (var i=0; i<links.length; i++) {
            links[i].setAttribute('class', '');
        }
        var num_tab_param=parseInt(this.getAttribute('data_num'));
        if (num_tab_param>=0) {
            refresh_tab_param(num_tab_param);
        }
        var this_el = e.originalTarget || e.srcElement;
        this_el.setAttribute('class', 'active');
        var curr_tab=document.getElementById(this_el.hash);
        curr_tab.setAttribute('class', 'active');
        var num = parseInt(this_el.hash.substr(4));
        var container_additional=document.getElementById('container_additional');
        if (num>=graph_list.length){
            container_additional.add_class('hide');
        }
        else {
            container_additional.remove_class('hide');
        }
        for (var i=0; i<graph_list.length; i++) {
            if (i==num) {
                var t_head=document.getElementById('table_title');
                var num_t_head=t_head.getElementsByTagName('span')[0];
                num_t_head.innerText=i+1;
                try {
                    if (tabs[1].params['IN'+(num+1)+'Info'][0]!=undefined && tabs[1].params['IN'+(num+1)+'Info'][0]!='') {
                        num_t_head.innerText+=' - '+tabs[1].params['IN'+(num+1)+'Info'][0];
                    }
                }
                catch (e) {

                }
                curr_graph = new Graph({
                    id: graph_list[i],
                    tick_x: 40,
                    tick_y: 5,
                    zoom_action: zoom_action
                });
                curr_graph.auto_refresh = button_set_auto.checked;
                get_and_draw(curr_graph, true);
                clearInterval(curr_interval);
                curr_interval = setInterval(function() {
                    get_and_draw(curr_graph);
                }, RefreshPeriod*1000);
            }
        }
    }
    for (var i=0; i<links.length; i++) {
        links[i].onclick = lnk_click;
        links[i].href='#tab'+ i.toString();
        links[i].idhref='link_tab'+ i.toString();
        links[i].onselectstart=function() {return false};
    }
    var tabs_content=document.getElementById('tabs_content');
//    var tabs_arr=tabs_content.getElementsByTagName('div');
    var tabs_arr=tabs_content.childNodes;
    var tabs_arr1=[];
    for (var i=0; i<tabs_arr.length; i++) {
        if (tabs_arr[i].tagName) {
            if (tabs_arr[i].tagName.toString().toLowerCase()=='div') {
                tabs_arr1.push(tabs_arr[i]);
            }
        }
    }
    tabs_arr=tabs_arr1;
    for (var i=0; i<tabs_arr.length; i++) {
        tabs_arr[i].id = '#tab'+ i.toString();
    }

    var button_set_auto = document.getElementById('set_auto');
    button_set_auto.onclick = function() {
        curr_graph.auto_refresh=this.checked;
        if (this.checked) {
            get_and_draw(curr_graph);
        }
    };

    var button_reset_data = document.getElementById('reset_data_btn');
    button_reset_data.onclick = function() {
        curr_graph.reset_zoom();
        get_and_draw(curr_graph, true);
        var button_set_auto = document.getElementById('set_auto');
        button_set_auto.checked=false;
        curr_graph.no_zoom=false;
        curr_graph.auto_refresh=false;
    };

    var button_reset_zoom = document.getElementById('reset_zoom_btn');
    button_reset_zoom.onclick = function() {
        curr_graph.reset_zoom();
    };

    var button_grid = document.getElementsByName('grid');
    button_grid[0].onclick = button_grid[1].onclick = function() {
        var num_graph=parseInt(curr_graph.id.substr(5));
        try {
            tabs[0].params['IN'+num_graph+'Grid'][0]=this.value;
            edited['IN'+num_graph+'Grid']=this.value.toString();
            var btn_submit=document.getElementById('btn_submit_3');
            save_data_tab(0);
        }
        catch (e) {};
        curr_graph.no_zoom=true;
        curr_graph.reset_zoom();
        get_and_draw(curr_graph, true);
    };

    function change_value(e) {
        var param=find_param(this.id.substr(3));
        var value=this.value;
        if (this.type=='checkbox') {
            value=(this.checked)?1:2;
        }
        if (param[0]!=value){
            this.add_class('edited');
            edited[this.id.substr(3)]=value.toString();
        }
        else {
            this.remove_class('edited');
            delete edited[this.id.substr(3)];
        }
        if (this.type=='text') {
            if (e.keyCode==13) {
                var num_tab=-1;
                for (var i=0; i<tabs.length; i++){
                    if (tabs[i].params[this.id.substr(3)]!=undefined) {
                        num_tab=i;
                    }
                }
                if (num_tab>-1) {
                    save_data_tab(num_tab);
                }
            }
        }
    }
    for (var i in tabs) {
        var form=document.getElementById('form_type_'+ tabs[i].val);
        var inputs=form.getElementsByTagName('input');
        for (var i=0; i<inputs.length; i++) {
            inputs[i].onkeyup=change_value;
            if (inputs[i].type=='checkbox') {
                inputs[i].onchange=change_value;
            }
        }
        var selects=form.getElementsByTagName('select');
        for (var i=0; i<selects.length; i++) {
            selects[i].onchange=change_value;
        }
    }
    var buttons_reset=document.getElementsByClassName('button_reset');
    for (var i=0; i<buttons_reset.length; i++) {
        buttons_reset[i].onclick=reset_params;
    }
    function button_refresh() {
        refresh_tab_param(parseInt(this.getAttribute('data_num')));
    }
    var buttons_refresh=document.getElementsByClassName('button_refresh');
    for (var i=0; i<buttons_refresh.length; i++) {
        buttons_refresh[i].onclick=button_refresh;
    }
    function save_data_tab(num_tab) {
        if (num_tab!=NaN && num_tab!=undefined && JSON.stringify(edited)!='{}') {
            ajax({
                url: desc_url,
                type: 'GET',
                data: 'set&t='+tabs[num_tab].val+'&s='+JSON.stringify(edited),
                success: function(data) {
                    var obj=check_error(data);
                    if (!obj) {
                        return;
                    }
                    if (obj=='') {
                        var error_div=document.getElementById('error_div');
                        var error_span=error_div.getElementsByTagName('span')[0];
                        error_span.innerHTML='Сервер вернул пустые данные';
                        error_div.remove_class('hide');
                        var color_div=document.getElementById('error_text');
                        color_div.className='red';
                        return;
                    }
                    edited={};
                    for (var param in obj) {
                        set_param(num_tab, param, obj[param]);
                        if (num_tab==0 && param.toString()=='RefreshPeriod') {
                            RefreshPeriod=parseInt(obj[param]);
                        }
                    }
                    if (num_tab==0) {
                        if (curr_interval) {
                            clearInterval(curr_interval);
                            get_and_draw(curr_graph);
                            curr_interval = setInterval(function() {
                                get_and_draw(curr_graph);
                            }, RefreshPeriod*1000);
                        }
                        else {
                            if (curr_graph) {
                                get_and_draw(curr_graph);
                            }
                        }
                    }
                }
            });
        }
    }
    function save_data(e) {
        var num_tab=parseInt(this.getAttribute('data_num'));
        save_data_tab(num_tab);
    }
    var buttons_submit=document.getElementsByClassName('button_submit');
    for (var i=0; i<buttons_submit.length; i++) {
        buttons_submit[i].onmouseup=save_data;
    }
}

ajax({
    url: desc_url,
    type: 'GET',
    data: 'descr',
    success: function(data) {
        var obj=check_error(data);
        if (!obj) {
            return;
        }
        var prop;
        var form_text='';
        var input_text='';
        for (var i=0; i<tabs.length; i++) {
            tabs[i].params = obj[tabs[i].val];
            var tabs_ul=document.getElementById('tabs');
            tabs_ul.innerHTML+='<li><a href="#" data_num="'+i+'">'+tabs[i].name+'</a></li>';
            var tabs_content=document.getElementById('tabs_content');
            form_text='';
            for (prop in obj[tabs[i].val]) {
                if (obj[tabs[i].val][prop][1].substr(0,2)!='rw') {
                    input_text='<span>'+obj[tabs[i].val][prop][0]+'</span>';
                }
                else {
                    if (obj[tabs[i].val][prop].length<5) {
                        input_text='<input type="text" id="id_'+prop+'" value="'+obj[tabs[i].val][prop][0]+'" />';
                    }
                    else {
                        if (obj[tabs[i].val][prop].length==5) {
                            if (obj[tabs[i].val][prop][4]['1']=='On' && obj[tabs[i].val][prop][4]['2']=='Off') {
                                var checked='';
                                if (obj[tabs[i].val][prop][0]==1) {
                                    checked='checked="checked"';
                                }
                                input_text='<input type="checkbox" '+checked+' id="id_'+prop+'" />';
                            }
                            else {
                                input_text='<select id="id_'+prop+'">';
                                var prop1;
                                var checked='';
                                for (prop1 in obj[tabs[i].val][prop][4]) {
                                    if (obj[tabs[i].val][prop][0]==prop1) {
                                        checked='selected="selected"';
                                    }
                                    else {
                                        checked='';
                                    }
                                    input_text+='<option '+checked+' value="'+prop1+'">'+obj[tabs[i].val][prop][4][prop1]+'</option>';
                                }
                                input_text+='</select>';
                            }
                        }
                        else {
                            input_text='';
                        }
                    }
                }
                form_text+='<tr><td><label for="id_'+prop+'">'+prop+'</label></td><td>'+input_text+'</td><td>'+obj[tabs[i].val][prop][3]+'</td></tr>';
            }
            var button_text='<label class="button button_refresh" for="refresh_'+tabs[i].val+'" data_id="'+tabs[i].val+'" data_num="'+i+'"><img src="img/refresh.png" alt="" width="16" height="16" id="refresh_'+tabs[i].val+'"/><span>Обновить</span></label>';
            button_text+='<label class="button button_reset" for="reset_'+tabs[i].val+'" id="btn_submit_'+tabs[i].val+'"><img src="img/remove.png" alt="" width="16" height="16" id="reset_'+tabs[i].val+'"/><span>Отменить</span></label>'
            button_text+='<label class="button button_submit" for="submit_'+tabs[i].val+'" id="btn_submit_'+tabs[i].val+'"  data_num="'+i+'"><img src="img/save.png" alt="" width="16" height="16" id="submit_'+tabs[i].val+'"/><span>Сохранить</span></label>';
            tabs_content.innerHTML+='<div><div class="container"><form action="'+desc_url+'" id="form_type_'+tabs[i].val+'"><table class="form_table"><thead><th>Параметр</th><th>Значение</th><th>Описание</th></thead>'+form_text+'</table>'+button_text+'</form></div></div>';
            refresh_tab_param(i);
        }
        main();
    },
    onerror: function(data) {
//        alert(data);
    }
});

function find_param(name) {
    for (var i=0; i<tabs.length; i++) {
        if (tabs[i].params[name]) {
            return tabs[i].params[name];
        }
    }
    return false;
}

function set_param(i, name, val) {
    if (tabs[i].params[name]) {
        tabs[i].params[name][0]=val;
    }
    var element=document.getElementById('id_'+name);
    if (element) {
        if (element.tagName.toLowerCase()=='select') {
            element.value=tabs[i].params[name][0];
        }
        else {
            if (element.type=='checkbox') {
                element.checked=(tabs[i].params[name][0]=='1');
            }
            else {
                element.value=tabs[i].params[name][0];
            }
        }
        element.remove_class('edited');
    }
    return false;
}

function reset_params() {
    var element;
    for (var i=0; i<tabs.length; i++) {
        for (var param in tabs[i].params) {
            element=document.getElementById('id_'+param);
            if (element) {
                if (element.tagName.toLowerCase()=='select') {
                    element.value=tabs[i].params[param][0];
                }
                else {
                    if (element.type=='checkbox') {
                        element.checked=tabs[i].params[param][0]=='1';
                    }
                    else {
                        element.value=tabs[i].params[param][0];
                    }
                }
                element.remove_class('edited');
            }
        }
    }
    edited={};
}

function refresh_tab_param(i) {
    ajax({
        url: desc_url,
        type: 'GET',
        data: 'get&t='+tabs[i].val,
        success: function(data) {
            var obj=check_error(data);
            if (!obj) {
                return;
            }
            edited={};
            for (var param in obj) {
                if (i==0 && param.toString()=='RefreshPeriod') {
                    RefreshPeriod=parseInt(obj[param]);
                }
                try {
                    set_param(i, param, obj[param]);
                    if (param.toString()=='Slot') {
                        var slot_val=document.getElementById('Slot');
                        slot_val.innerText='('+obj[param]+')';
                    }
                }
                catch (e) {

                }
            }
            if (i==0) {
                if (curr_interval) {
                    clearInterval(curr_interval);
                    get_and_draw(curr_graph);
                    curr_interval = setInterval(function() {
                        get_and_draw(curr_graph);
                    }, RefreshPeriod*1000);
                }
                else {
                    if (curr_graph) {
                        get_and_draw(curr_graph);
                    }
                }
            }
            if (i==1) {
                var t_head=document.getElementById('table_title');
                var num_t_head=t_head.getElementsByTagName('span')[0];
                num_t_head.innerText=1;
                try {
                    if (tabs[1].params['IN1Info'][0]!=undefined && tabs[1].params['IN1Info'][0]!='') {
                        num_t_head.innerText+=' - '+tabs[1].params['IN1Info'][0];
                    }
                }
                catch (e) {

                }
            }
        },
        onerror: function(data) {
//            refresh_tab_param(i);
        }
    });
}