/*
    It`s free library
    done Gorkovenko Dmitry, gorkovenko.dmitry@gmail.com
 */
function clearSelection() {
    if (window.getSelection) {
        window.getSelection().removeAllRanges();
    } else { // старый IE
        document.selection.empty();
    }
}
function getOffsetSum(elem) {
    var top=0, left=0;
    while(elem) {
        top = top + parseFloat(elem.offsetTop);
        left = left + parseFloat(elem.offsetLeft);
        elem = elem.offsetParent;
    }
    return {top: Math.round(top), left: Math.round(left)};
}
function getOffsetRect(elem) {
    // (1)
    var box = elem.getBoundingClientRect();
    // (2)
    var body = document.body;
    var docElem = document.documentElement;
    // (3)
    var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
    var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;
    // (4)
    var clientTop = docElem.clientTop || body.clientTop || 0;
    var clientLeft = docElem.clientLeft || body.clientLeft || 0;
    // (5)
    var top  = box.top +  scrollTop - clientTop;
    var left = box.left + scrollLeft - clientLeft;
    return { top: Math.round(top), left: Math.round(left) };
}
function getOffset(elem) {
    if (elem.getBoundingClientRect) {
        // "правильный" вариант
        return getOffsetRect(elem);
    } else {
        // пусть работает хоть как-то
        return getOffsetSum(elem);
    }
}
function createRequest() {
    // Создание объекта XMLHttpRequest отличается для Internet Explorer и других обозревателей, поэтому для совместимости эту операцию приходиться дублировать разными способами
    var req;
    if (window.XMLHttpRequest) req = new XMLHttpRequest();		// normal browser
    else if (window.ActiveXObject) {							// IE
        try {
            req = new ActiveXObject('Msxml2.XMLHTTP');			// IE разных версий
        } catch (e){}											// может создавать
        try {													// объект по разному
            req = new ActiveXObject('Microsoft.XMLHTTP');
        } catch (e){}
    }
    return req;
}

function Graph(args) {
    this.id = args.id;
    this.canvas = document.getElementById(this.id);
    this.data_url = this.canvas.getAttribute('data-url');
    this.canvas.ctx = this.ctx = this.canvas.getContext('2d');
    this.canvas.shift_x = this.shift_x = 40;
    this.canvas.shift_y = this.shift_y = 80;

    this.canvas._width = this._width = this.canvas.offsetWidth - this.shift_x;
    this.canvas._height = this._height = this.canvas.offsetHeight - this.shift_y;

    this.tick_x = args.tick_x;
    this.tick_y = args.tick_y;

    this.canvas.x1 = this.shift_x;
    this.canvas.x2 = this._width;
    this.canvas.y1 = this.shift_y;
    this.canvas.y2 = this._height;

    this.canvas.canvas = this.canvas;
    this.canvas.parent_class = this;
    this.no_zoom=true;
    this.grid_val=2;
    this.zoom_action=args.zoom_action;
    var canvas = this.canvas;

    this.clear_rect();
    canvas.ctx.save();
    canvas.ctx.translate((canvas.parent_class._width+this.shift_x)/2-150, (this._height+this.shift_y)/2);
    canvas.ctx.fillStyle = '#369';
    canvas.ctx.font = '12px sans-serif';
    canvas.ctx.textBaseline = 'top';
    canvas.ctx.fillText('Подождите, идет загрузка данных', 0, 0);
    canvas.ctx.restore();

    var start_mouse, end_mouse=false;
    this.start_select=false;
    function set_zoom() {
        end_mouse = canvas.get_mouse(end_mouse);
        canvas.parent_class.no_zoom=false;
        var x1, x2, y1, y2;
        canvas.x1 = Math.min(start_mouse[0], end_mouse[0]);
        canvas.x2 = Math.max(start_mouse[0], end_mouse[0]);
        canvas.y1 = Math.min(start_mouse[1], end_mouse[1]);
        canvas.y2 = Math.max(start_mouse[1], end_mouse[1]);
        if (Math.abs(canvas.x1-canvas.x2)<10 && Math.abs(canvas.y1-canvas.y2)<10) {
            console.log('слишком маленькая область');
            canvas.parent_class.start_select=false;
            return;
        }
        var data =[];
        x1 = (canvas.x1-canvas.parent_class.shift_x) * (canvas.parent_class.max_min.x_max-canvas.parent_class.max_min.x_min) / (canvas.parent_class._width) + canvas.parent_class.max_min.x_min;
        x2 = (canvas.x2-canvas.parent_class.shift_x) * (canvas.parent_class.max_min.x_max-canvas.parent_class.max_min.x_min) / (canvas.parent_class._width) + canvas.parent_class.max_min.x_min;
        y1 = (canvas.parent_class._height+canvas.parent_class.shift_y/2-canvas.y1) * (canvas.parent_class.max_min.y_max-canvas.parent_class.max_min.y_min) / (canvas.parent_class._height) + canvas.parent_class.max_min.y_min;
        y2 = (canvas.parent_class._height+canvas.parent_class.shift_y/2-canvas.y2) * (canvas.parent_class.max_min.y_max-canvas.parent_class.max_min.y_min) / (canvas.parent_class._height) + canvas.parent_class.max_min.y_min;
        var el;
        for (var i=0; i<canvas.parent_class.data.length; i++) {
            el=canvas.parent_class.data[i];
            if (el[0]>=x1 && el[0]<=x2) {
                data.push(el);
            }
        }
        if (data.length>5) {
            canvas.parent_class.auto_refresh=false;
            canvas.parent_class.data = data;
            canvas.parent_class.draw();
            canvas.parent_class.zoom_action();
            canvas.parent_class.start_select=false;
        }
        else {
            console.log('точек попало: '+data.length);
            canvas.parent_class.start_select=false;
        }
    }
    this.canvas.get_mouse = this.get_mouse;
    this.canvas.onmousedown = function(e) {
        this.parent_class.no_zoom=false;
        start_mouse = canvas.get_mouse(e);
        this.parent_class.start_select=true;
        document.onmouseup = function(e) {
            if (canvas.parent_class.start_select) {
                end_mouse = e;
                set_zoom();
                canvas.parent_class.draw();
                canvas.parent_class.start_select=false;
            }
        }
    };

    this.canvas.onmousemove = function(e) {
        if (canvas.parent_class.data!=undefined) {
            var curr_pos = canvas.get_mouse(e);
            if (this.parent_class.start_select) {
                this.parent_class.draw();
                this.parent_class.ctx.fillStyle="rgba(43, 135, 214, 0.45)";
                var x1, x2, y1, y2;
                x1=start_mouse[0];
                x2=curr_pos[0];
                y1=start_mouse[1];
                y2=curr_pos[1];
                this.parent_class.ctx.fillRect(start_mouse[0], start_mouse[1], x2-x1, y2-y1);
            }

            var x, y;
            x = (curr_pos[0]-canvas.parent_class.shift_x) * (canvas.parent_class.max_min.x_max-canvas.parent_class.max_min.x_min) / (canvas.parent_class._width) + canvas.parent_class.max_min.x_min;
            y = (canvas.parent_class._height+canvas.parent_class.shift_y/2-curr_pos[1]) * (canvas.parent_class.max_min.y_max-canvas.parent_class.max_min.y_min) / (canvas.parent_class._height) + canvas.parent_class.max_min.y_min;
            canvas.ctx.fillStyle="#5bb3ee";
            canvas.ctx.fillRect(canvas.parent_class._width-55, 45, 90, 50);
            canvas.ctx.save();
            canvas.ctx.translate(canvas.parent_class._width-45, 55);
            canvas.ctx.fillStyle = '#369';
            canvas.ctx.font = '12px sans-serif';
            canvas.ctx.textBaseline = 'top';
            canvas.ctx.fillText('x: '+(x).toFixed(3), 0, 0);
            canvas.ctx.restore();

            canvas.ctx.save();
            canvas.ctx.translate(canvas.parent_class._width-45, 75);
            canvas.ctx.fillStyle = '#369';
            canvas.ctx.font = '12px sans-serif';
            canvas.ctx.textBaseline = 'top';
            canvas.ctx.fillText('y: '+(y).toFixed(3), 0, 0);
            canvas.ctx.restore();
        }
    };

    this.canvas.ondblclick = function () {
        clearSelection();
        canvas.parent_class.reset_zoom();
    }
}

Graph.prototype.reset_zoom = function() {
    this.data = this.old_data;
    this.draw();
    this.no_zoom = true;
};

Graph.prototype.get_mouse = function(e) {
    var offset_canvas = getOffset(this.canvas);
    var x = e.clientX - offset_canvas.left + window.pageXOffset;
    var y = e.clientY - offset_canvas.top + window.pageYOffset;
    return [
        Math.min(this._width+this.shift_x-3, Math.max(this.shift_x, x)),
        Math.min(this._height+this.shift_y/2-3, Math.max(this.shift_y/2+3, y))
    ];
};

Graph.prototype.draw = function() {
    if (this.data == undefined) {return}
    this.clear_rect();
    this.max_min = this.get_max_min();
    this.draw_grid();
    var x, y, x2, y2;
    this.ctx.save();
    this.ctx.translate(this.shift_x, this.shift_y/2);
    for (var i=0; i<this.data.length; i++) {
        x = this._width * (this.data[i][0]-this.max_min.x_min) / (this.max_min.x_max-this.max_min.x_min);
        y = this._height - this._height * (this.data[i][1]-this.max_min.y_min) / (this.max_min.y_max-this.max_min.y_min);
        this.ctx.beginPath();
        this.ctx.fillStyle="#FF6699";
        this.ctx.moveTo(x, y);
        this.ctx.arc(x, y, 2, 0, Math.PI*2, true);
        this.ctx.fill();
        if (i>0) {
            this.ctx.beginPath();
            this.ctx.strokeStyle = "#FF6699";
            this.ctx.moveTo(x2, y2);
            this.ctx.lineTo(x, y);
            this.ctx.stroke();
        }
        x2=x;
        y2=y;
    }
    this.ctx.restore();
};

Graph.prototype.clear_rect = function() {
    this.ctx.clearRect(0, 0,this._width+this.shift_x,this._height+this.shift_y);
    this.ctx.fillStyle="#fffdf6";
    this.ctx.fillRect(0, 0,this._width+this.shift_x,this._height+this.shift_y);
};

Graph.prototype.get_max_min = function() {
    var x_max, x_min, y_max, y_min;
    for (var i=0; i<this.data.length; i++) {
        if (i==0) {
            x_max = this.data[i][0];
            x_min = this.data[i][0];
            y_max = this.data[i][1];
            y_min = this.data[i][1];
        }
        else {
            if (this.data[i][0]>x_max) x_max = this.data[i][0];
            if (this.data[i][0]<x_min) x_min = this.data[i][0];
            if (this.data[i][1]>y_max) y_max = this.data[i][1];
            if (this.data[i][1]<y_min) y_min = this.data[i][1];
        }
    }
    x_max = x_max + (x_max - x_min) / 100;
    y_max = y_max + (y_max - y_min) / 10;
    return {x_max: x_max, x_min: x_min, y_max: y_max, y_min: y_min};
};

Graph.prototype.draw_grid = function() {

    var h_step = (this.max_min.x_max - this.max_min.x_min) / (this.tick_x+1);
    var x;
    var i;
    for (i=0; i<this.tick_x; i++) {
        this.ctx.save();
        this.ctx.translate(this.shift_x,this.shift_y/2);
        this.ctx.beginPath();
        this.ctx.strokeStyle = "rgba(100,100,100,0.3)";
        x = this._width * (h_step*(i+1)) / (this.max_min.x_max-this.max_min.x_min);
        this.ctx.moveTo(x, 0);
        this.ctx.lineTo(x, this._height);
        this.ctx.stroke();
        this.ctx.restore();

        this.ctx.save();
        this.ctx.translate(this.shift_x+x-7, this._height+this.shift_y);
        this.ctx.rotate(-Math.PI/2);
        this.ctx.fillStyle = '#369';
        this.ctx.font = '12px sans-serif';
        this.ctx.textBaseline = 'top';
        this.ctx.fillText ((this.max_min.x_min + h_step*(i+1)).toFixed(1), 0, 0);
        this.ctx.restore();
    }

    var w_step = (this.max_min.y_max - this.max_min.y_min) / (this.tick_y + 1);
    var y;
    for (i=0; i<this.tick_y+1; i++) {
        this.ctx.save();
        this.ctx.translate(this.shift_x, this.shift_y/2);
        this.ctx.beginPath();
        this.ctx.strokeStyle = "rgba(100,100,100,0.3)";
        y = this._height - this._height * (w_step*i) / (this.max_min.y_max-this.max_min.y_min);
        this.ctx.moveTo(0, y);
        this.ctx.lineTo(this._width, y);
        this.ctx.stroke();
        this.ctx.restore();

        this.ctx.save();
        this.ctx.translate(0, this.shift_y/2+y-7);
        this.ctx.fillStyle = '#369';
        this.ctx.font = '12px sans-serif';
        this.ctx.textBaseline = 'top';
        this.ctx.fillText((this.max_min.y_min + w_step*i).toFixed(1), 0, 0);
        this.ctx.restore();
    }

    //    axe X
    this.ctx.beginPath();
    this.ctx.strokeStyle = "#000";
    this.ctx.moveTo(this.shift_x, this._height+this.shift_y/2);
    this.ctx.lineTo(this._width+this.shift_x, this._height+this.shift_y/2);
    this.ctx.stroke();

    this.ctx.save();
    this.ctx.translate(this._width+10, this._height+this.shift_y/4);
    this.ctx.fillStyle = '#000';
    this.ctx.font = '12px sans-serif bold';
    this.ctx.textBaseline = 'top';
    this.ctx.fillText('λ, нм', 0, 0);
    this.ctx.restore();

    //    axe Y
    this.ctx.beginPath();
    this.ctx.strokeStyle = "#000";
    this.ctx.moveTo(this.shift_x, this._height+this.shift_y/2);
    this.ctx.lineTo(this.shift_x, this.shift_y/2);
    this.ctx.stroke();

    this.ctx.save();
    this.ctx.translate(this.shift_x+5, 45);
    this.ctx.fillStyle = '#000';
    this.ctx.font = '12px sans-serif bold';
    this.ctx.textBaseline = 'top';
    this.ctx.fillText('P, дБм', 0, 0);
    this.ctx.restore();

    //    axe channel
    this.ctx.beginPath();
    this.ctx.strokeStyle = "#000";
    this.ctx.moveTo(this.shift_x, this.shift_y/2);
    this.ctx.lineTo(this._width+this.shift_x, this.shift_y/2);
    this.ctx.stroke();
    var channels = [];
    var f, l, channel_name, channel_num;
    channel_num=1;
    for (i=0; i<144; i++) {
        var el=[];
        el[0]=i;
        f=190.1+i*0.05;
        l=299792.458/f;
        if (i%2==0) {
            channel_name = 'C' + channel_num;
        }
        else {
            channel_name = 'H' + channel_num;
            channel_num++;
        }
        if (this.grid_val==1 && i%2!=0) {
            continue;
        }
        el[1]=channel_name;
        el[2]=l;
        if (l>=this.max_min.x_min && l<=this.max_min.x_max) {
            channels.push(el);
        }
    }
    if (channels.length>0) {
        var step = Math.round(channels.length/(this.tick_x/2));
        if (channels.length>this.tick_x) {
            var new_arr=[];
            for(i=0; i<channels.length; i++){
                if (i%step==0) {
                    new_arr.push(channels[i]);
                }
            }
            channels=new_arr;
        }
        for(i=0; i<channels.length; i++){
            this.ctx.save();
            this.ctx.translate(this.shift_x, this.shift_y/2);
            this.ctx.beginPath();
            this.ctx.strokeStyle = '#43BB48';
            x = this._width * (channels[i][2]-this.max_min.x_min) / (this.max_min.x_max-this.max_min.x_min);
            this.ctx.moveTo(x, 0);
            this.ctx.lineTo(x, this._height);
            this.ctx.stroke();
            this.ctx.restore();

            this.ctx.save();
            this.ctx.translate(this.shift_x+x-7, this.shift_y/2-5);
            this.ctx.rotate(-Math.PI/2);
            this.ctx.fillStyle = '#43BB48';
            this.ctx.font = '12px sans-serif';
            this.ctx.textBaseline = 'top';
            this.ctx.fillText (channels[i][1], 0, 0);
            this.ctx.restore();
        }
    }
};

function ajax(args) {
    var xmlhttp=createRequest();
    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState==4 && xmlhttp.status==200){
            args.success(xmlhttp.responseText);
        }
        else {
            if (args.onerror!=undefined) {
                args.onerror(xmlhttp.responseText);
            }
        }
    };
    var date = new Date();
    var ticks = date.getTime();
    xmlhttp.open(args.type, args.url+'?no_cache='+ticks+'&'+encodeURI(args.data), true);
    xmlhttp.send();
}